//
//  ViewController.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/15/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, MKMapViewDelegate, LocationManagerDelegate{
 
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager:LocationManagerProtocol = LocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }
    
    func locationManager(didUpdateLocation location: CLLocation) {
        let longitudeText = location.coordinate.longitude
        let latitudeText = location.coordinate.latitude
        longitudeLabel.text = String(longitudeText)
        latitudeLabel.text = String(latitudeText)
        
        //present on map kit for now
        let coordinate = MKCoordinateRegionMakeWithDistance(location.coordinate, 1000, 1000)
        mapView.setRegion(coordinate, animated: true)
    }
}

extension ViewController: LocationAuthorizationDelegate{
    func locationAuthorizationFailed(){
        print("------- failed -------")
    }
    
    func locationAuthorizationSuccesfull(){
        locationManager.startTrackingUserLocation()
        print("------- succesfull -------")
    }
    
}



