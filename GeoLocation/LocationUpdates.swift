//
//  LocationUpdates.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/16/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation

class LocationUpdates: NSObject, LocationUpdatesProtocol{
    
    let locationManager = CLLocationManager()
    var locationUpdatesDelegate: LocationUpdatesDelegate?
    
    //default location for previous location
    var previousLocation:CLLocation = CLLocation(latitude: 00.0, longitude: 00.0)
    
    init(accuracy: CLLocationAccuracy = kCLLocationAccuracyBest, distance:CLLocationDistance = kCLDistanceFilterNone) {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = accuracy
        locationManager.distanceFilter = distance
    }
    
    func startUpdatingLocation(){
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation(){
        locationManager.stopUpdatingLocation()
    }
    
}
extension LocationUpdates: CLLocationManagerDelegate{
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard locations.count > 0 else{
            return
        }
        let lastLocation = locations.last!
        //if user location isn't changed don't call delegate's methods
        if isLocationChanged(first: previousLocation, second: lastLocation){
            previousLocation = lastLocation
            locationUpdatesDelegate?.locationUpdates(didUpdateLocation: lastLocation)
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        locationUpdatesDelegate?.locationManagerDidPauseLocationUpdates(manager)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //should handle error here
        locationUpdatesDelegate?.locationUpdates(didFailWithError: error)
    }
    
    
    
    private func isLocationChanged(first:CLLocation, second:CLLocation) -> Bool{
        if first.coordinate.latitude == second.coordinate.latitude &&
            first.coordinate.longitude == second.coordinate.longitude{
            return false
        }
        return true
    }
}
