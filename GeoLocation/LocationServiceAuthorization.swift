//
//  LocationServiceAuthorization.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/16/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class LocationServiceAuthorization: NSObject, LocationAuthorizationProtocol{
    var locationAuthorizationDelegate: LocationAuthorizationDelegate?

    let locationManager  = CLLocationManager()
    
    func requestAlwaysAuthorization(){
        if CLLocationManager.authorizationStatus() != .authorizedAlways{
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func requestWhenInUseAuthorization(){
        if  CLLocationManager.authorizationStatus() != .authorizedWhenInUse{
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func isLocationServiceEnabled() -> Bool{
        if CLLocationManager.locationServicesEnabled(){
            return true
        }
        return false
    }
    
    
    func isLocationServiceAuthorized() -> Bool{
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways || status == .authorizedWhenInUse{
            return true
        }
        return false
    }
    
}

extension LocationServiceAuthorization: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse{
            locationAuthorizationDelegate?.locationAuthorizationSuccesfull()
        }else{
            locationAuthorizationDelegate?.locationAuthorizationFailed()
        }
    }
}
