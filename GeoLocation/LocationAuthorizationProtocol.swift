//
//  LocationAuthUserMethodsDelegate.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol LocationAuthorizationProtocol{
    var locationAuthorizationDelegate:LocationAuthorizationDelegate? {get set}
    
    func isLocationServiceEnabled() -> Bool
    func isLocationServiceAuthorized() -> Bool
    func requestAlwaysAuthorization()
    func requestWhenInUseAuthorization()

}

// Optional Methods
extension LocationAuthorizationProtocol{

    func isLocationServiceEnabled() -> Bool {return true}
    func isLocationServiceAuthorized() -> Bool {return true}
    
    func requestAlwaysAuthorization(){}
    func requestWhenInUseAuthorization(){}

}
